import { Request, Response } from "express";
import customerValidation from "../validations/customer"
import Customer from "../models/Customer"; 

// Index customer
export const index = (req: Request, res: Response) => {
  const getCustomers = async () => {
    try {
      const customers = await Customer.find({});
      return res.send(customers);
    } catch (err) {
      return res.status(400).send(err.message);
    }
  }

  getCustomers();
}

// Create customer
export const create = (req: Request, res: Response) => {
  // Input validation
  const validationError = customerValidation.validate(req.body);
  if (validationError) {
    return res.status(422).send(validationError);
  }

  // Instantiate customer model
  const newCustomer = new Customer({
    name: req.body.name,
    phone: req.body.phone,
    isGold: req.body.isGold
  });

  // Save customer
  const createCustomer = async () => {
    try {
      const customer = await newCustomer.save();
      return res.send(customer);
      
    } catch (err) {
      return res.status(400).send(err.message);
    }
  }

  createCustomer();
}

// Get customer by ID
export const findById = (req: Request, res: Response) => {
  const getCustomer = async () => {
    try {
      const customer = await Customer.findById(req.params.id);
      if (!customer) {
        return res.status(404).send('Customer not found');
      }
      return res.send(customer);

    } catch (err) {
      return res.status(400).send(err.message);
    }
  } 

  getCustomer();
}

// Update customer
export const update = (req: Request, res: Response) => {
  const updateCustomer = async () => {
    try {
      let customer = await Customer.findById(req.params.id);

      if (!customer) {
        return res.status(404).send('Customer not found');
      }

      req.body.name ? customer.name = req.body.name : '';
      req.body.phone ? customer.phone = req.body.phone : '';
      req.body.isGold ? customer.isGold = req.body.isGold : '';
      customer = await customer.save();
      return res.send(customer);

    } catch (err) {
      return res.status(400).send(err.message);
    }
  }

  updateCustomer();
};

// Delete customer
export const remove = (req: Request, res: Response) => {
  const removeCustomer = async () => {
    try {
      const customer = await Customer.findByIdAndRemove(req.params.id);
      if (!customer) {
        return res.status(404).send('Customer not found');
      }

      return res.send(customer);
      
    } catch (err) {
      return res.status(400).send(err.message);
    }
  }

  removeCustomer();
};