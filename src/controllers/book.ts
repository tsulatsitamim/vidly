import { Request, Response } from "express";
import Genre from "../models/Genre";
import bookValidation from "../validations/book";
import Book from "../models/Book";

// Index all book
export const index = async (req: Request, res: Response) => {
  try {
    const books = await Book.find({});
    return res.send(books);
  } catch (err) {
    return res.status(400).send(err.message);
  }
}

// Create a new book
export const create = (req: Request, res: Response) => {
  // Validate user request
  const valdation = bookValidation.create(req.body);
  if (valdation) {
    return res.status(422).send(valdation);
  }

  const createBook = async () => {
    try {
      // check if existing book exist
      let book = await Book.findOne({ name: req.body.name });
      if (book) {
        return res.status(422).send(('Book with the same name already exist.'));
      }

      // check if existing genre exist
      let genre = await Genre.findOne({ name: req.body.genre });
      if (!genre) {
        genre = new Genre({ name: req.body.genre });
        await genre.save();
      }

      // Create and response a new book
      book = new Book({
        title: req.body.title,
        genre: {
          _id: genre._id,
          name: genre.name
        },
        numberInStock: parseInt(req.body.numberInStock),
        dailyRentStock: parseInt(req.body.dailyRentStock)
      });
      await book.save()
      return res.send(book);

    } catch (err) {
      return res.status(400).send(err.message);
    }
  }

  createBook();
}