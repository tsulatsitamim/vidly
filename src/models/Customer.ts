import {default as mongoose, model} from "mongoose";

// Customer type
export type TCustomer = mongoose.Document & {
  name: string,
  phone: string,
  isGold: boolean
}

// Customer schema
const customerSchema = new mongoose.Schema({
  name: { type: String, required: true },
  phone: { type: String, required: true },
  isGold: { type: Boolean, default: false },
});

// Export customer model
export default mongoose.model<TCustomer>('Customer', customerSchema);