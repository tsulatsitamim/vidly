import mongoose, { mongo } from "mongoose";
import { TGenre, genreSchema } from "./Genre";

// Book document
export type TBook = mongoose.Document & {
  title: string,
  genre: TGenre,
  numberInStock: number,
  dailyRentStock: number
}

// Book schema
const bookSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    trim: true,
    minlength: 5,
  },
  genre: genreSchema,
  numberInStock: Number,
  dailyRentStock: Number
});

// export model
export default mongoose.model<TBook>('Book', bookSchema);