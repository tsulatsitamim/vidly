import { default as Joi, SchemaMap } from "joi";
import { JoiErrorValidation } from "../common/types";

export default {
  create(payload: object){
    const schema: SchemaMap = {
      title: Joi.string().min(3).required(),
      genre: Joi.string(),
      numberInStock: Joi.number().default(0),
      dailyRentStock: Joi.number().default(0)
    }
    const result = Joi.validate(payload, schema, { abortEarly: false });

    if (result.error) {
      const errorMessage: JoiErrorValidation[] = result.error.details.map(x => ({
        path: x.path[0],
        message: x.message
      }));

      return errorMessage;
    }
  }
}