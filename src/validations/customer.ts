import Joi from "joi";

type errorMessageType = {
  name: string,
  message: string
}[];

export default {
  schema: {
    name: Joi.string().min(3).max(50).required(),
    phone: Joi.string().min(3).max(50).required(),
    isGold: Joi.boolean()
  },
  validate(payload: object) {
    const result = Joi.validate(payload, this.schema, { abortEarly: false });
    let errorMessage: errorMessageType;
    if (result.error) {
      errorMessage = result.error.details.map(x => ({
        name: x.path[0],
        message: x.message
      }));
      return errorMessage;
    }
  }
}