import Joi from "joi";

type errorMessageType = {
  name: string,
  message: string
}[];

export default {
  create: {
    schema: {
      name: Joi.string().required()
    },
    validate(payload: object){
      const result = Joi.validate(payload, this.schema, { abortEarly: false });
      let errorMessage: errorMessageType;
      if (result.error) {
        errorMessage = result.error.details.map(x => ({
          name: x.path[0],
          message: x.message
        }));
        return errorMessage;
      }
    }
  },
  update: {
    schema: {
      name: Joi.string().required()
    },
    validate(payload: object){
      const result = Joi.validate(payload, this.schema, { abortEarly: false });
      let errorMessage: errorMessageType;
      if (result.error) {
        errorMessage = result.error.details.map(x => ({
          name: x.path[0],
          message: x.message
        }));
        return errorMessage;
      }
    }
  }
}